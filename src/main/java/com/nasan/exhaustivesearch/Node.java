/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nasan.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author Kitty
 */
public class Node {

    private ArrayList<Integer> subPair1 = new ArrayList<Integer>();
    private ArrayList<Integer> subPair2 = new ArrayList<Integer>();

    public Node(ArrayList<Integer> arrPair1, ArrayList<Integer> arrPair2) {
        this.subPair1 = arrPair1;
        this.subPair2 = arrPair2;
    }

    public ArrayList<Integer> getArrPair1() {
        return subPair1;
    }

    public void setArrPair1(ArrayList<Integer> arrPair1) {
        this.subPair1 = arrPair1;
    }

    public ArrayList<Integer> getArrPair2() {
        return subPair2;
    }

    public void setArrPair2(ArrayList<Integer> arrPair2) {
        this.subPair2 = arrPair2;
    }
    
    

}
